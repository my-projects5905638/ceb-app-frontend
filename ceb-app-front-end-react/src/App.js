import React from 'react';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Home from './components/Home';
import NavBarComp from './components/NavBarComp.js';
import ShowBill from './components/ShowBill';
import MeterReaderForm from './components/MeterReaderForm';
import Footer from './components/Footer';
import About from './components/About';

function App() {
    return (
        <Router>
            <div>
                <NavBarComp />
                <div>
                    <Switch>
                        <Route exact path="/">
                            <Home />
                        </Route>
                        <Route path="/meterReader">
                            <MeterReaderForm />
                        </Route>
                        <Route path="/customer">
                            <ShowBill />
                        </Route>
                        <Route path="/customer">
                            <ShowBill />
                        </Route>
                        <Route path="/about">
                            <About/>
                        </Route>
                    </Switch>
                </div>
                <Footer/>
            </div>
        </Router>
    );
}

export default App;