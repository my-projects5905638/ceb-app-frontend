import React from 'react';
import '../images/footer-logo.png';
import '../css/Footer.css';
import {Link} from "react-router-dom";

function Footer(props) {
    return (
        <div className={"footer"}>
            <footer>
                <div className="container">
                    <div className="footer-content">
                        <div className="footer-links">
                            <ul>
                                <li><Link to={"/about"}>About</Link></li>
                                <li><Link to={"/"}>Home</Link></li>
                                <li>Contact</li>
                            </ul>
                        </div>
                    </div>
                    <div className="footer-info">
                        <p>&copy; 2023 Sachindu Srilal. All Rights Reserved.</p>
                    </div>
                </div>
            </footer>
        </div>
    );
}

export default Footer;