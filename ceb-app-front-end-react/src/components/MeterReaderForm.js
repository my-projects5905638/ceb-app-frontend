import React, {useEffect, useState} from 'react'
import '../css/MeterReaderForm.css';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import NavBarComp from "./NavBarComp";
import '../css/MeterReaderForm.css';
import '../css/MeterReaderForm.css';
import Axios from 'axios'
import Swal from "sweetalert2";

function MeterReaderForm() {
    const [account_id, setAccount] = useState('');
    const [meter_reading, setReading] = useState('');
    const [reading_date, setDate] = useState('');

    const handlePostClick = async () => {
        Swal.fire(
            'success'
        )
        try {
            const postData = {
                account: account_id,
                reading: meter_reading,
                date: reading_date,
            };

            const response = await Axios.post('http://localhost:8082/api/v1/customers', postData);
            console.log(response);

            if (response.status === 201) {
                Swal.fire(
                    'Good ',
                    'You clicked the button!',
                    'success'
                )
                console.log("success")

            } else {
                console.error('Error:', response.data.message);
            }
        } catch (error) {
            console.error('Error:', error.message);
        }
    };

    
    const validateAccountNumber = () => {
        const regex = /^\d{8}$/;
    
        if (!regex.test(account_id)) {
          alert('Enter 8 length account number');
          return false;
        }
    
        return true;
    };

    const handleClick = async () => {
        if (validateAccountNumber()) {
          await handlePostClick();
        }
    };


    return (
        <div>
            <div className={"reader-body"}>
                <div className={"cover"}>
                    <h1>Customer Bill</h1>
                    <Form>
                        <Form.Group className="details mb-3" >
                            <Form.Label>Account Number</Form.Label>
                            <Form.Control type="text" 
                                            placeholder="Enter account number"  
                                            value={account_id} onChange={(e) =>
                                setAccount(e.target.value)}
                                onKeyDown={(e) => {
                                    if (e.key === 'Enter') {
                                      handleClick();
                                    }
                                  }}
                                />
                        </Form.Group>

                        <Form.Group className="details mb-3" >
                            <Form.Label>Meter Reading</Form.Label>
                            <Form.Control type="number" 
                                            placeholder="Enter reading" 
                                            value={meter_reading} 
                                            onChange={(e) =>
                                            setReading(e.target.value)}
                                onKeyDown={(e) => {
                                    if (e.key === 'Enter') {
                                      handleClick();
                                    }
                                  }}
                                />
                         </Form.Group>

                        <Form.Group className="details mb-3" >
                            <Form.Label>Date</Form.Label>
                            <Form.Control type="date" 
                                            placeholder="Enter date" 
                                            value={reading_date} 
                                            onChange={(e) =>
                                setDate(e.target.value)}
                                onKeyDown={(e) => {
                                    if (e.key === 'Enter') {
                                      handleClick();
                                    }
                                  }}
                                />
                        </Form.Group>

                        <Button onClick={handleClick} 
                                variant="primary" 
                                type="submit">
                            Submit
                        </Button>
                    </Form>
                </div>
            </div>
        </div>


    );
}

export default MeterReaderForm;