import React ,{Router} from 'react';
import '../css/NavBarComp.css'
import {Link} from "react-router-dom";

function NavBarComp() {
    return (
           <div className={"nav-bar"}>
               <nav>
                   <ul>
                       <li><Link to={"/"}>Home</Link></li>
                       <li><Link to={"/meterReader"}>Meter-Reader</Link></li>
                       <li><Link to={"/customer"}>Customer</Link></li>
                   </ul>
               </nav>
           </div>
    );
}

export default NavBarComp;