import React from 'react'
import '../css/Home.css';
import '../images/home.jpg';

function Home() {
  
    return (
        <div>
            <div className="home">
                <main>
                    <div className='section-container'> 
                        <section className="welcome-section ">
                            <h1>Welcome To CEB System</h1>
                            <p>Manage your customers with ease.</p>
                        </section>
                        <section className="features-section">
                            <div className='logo'>
                                <img></img>
                            </div>
                            <ul>
                                <li><h3>Customer Profiles</h3></li>
                                <li><h3>Check Bill Data</h3></li>
                                <li><h3>Contact Regional Manager</h3></li>
                            </ul>
                        </section>
                    </div>
                </main>
            </div>
        </div>
    )
}

export default Home;